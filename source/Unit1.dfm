object Form1: TForm1
  Left = 195
  Top = 98
  BorderStyle = bsSingle
  Caption = '*.mdb -> *.txt TM creator'
  ClientHeight = 550
  ClientWidth = 640
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 175
    Width = 640
    Height = 355
    Caption = '[ Nastaveni zdroje ]'
    TabOrder = 0
    Visible = False
    object Label4: TLabel
      Left = 5
      Top = 20
      Width = 147
      Height = 13
      Caption = 'Tabulka se zdrojovym jazykem:'
    end
    object Label5: TLabel
      Left = 245
      Top = 20
      Width = 129
      Height = 13
      Caption = 'Tabulka s cilovym jazykem:'
    end
    object Label6: TLabel
      Left = 5
      Top = 60
      Width = 209
      Height = 13
      Caption = 'Sloupec ID zdrojoveho jazyka a hodnota ID:'
    end
    object Label7: TLabel
      Left = 245
      Top = 60
      Width = 197
      Height = 13
      Caption = 'Sloupec ID ciloveho jazyka a hodnota ID:'
    end
    object Label8: TLabel
      Left = 5
      Top = 105
      Width = 123
      Height = 13
      Caption = 'Sloupec zdrojoveho textu:'
    end
    object Label9: TLabel
      Left = 245
      Top = 105
      Width = 111
      Height = 13
      Caption = 'Sloupec ciloveho textu:'
    end
    object Image1: TImage
      Left = 72
      Top = 145
      Width = 150
      Height = 55
      Center = True
      Picture.Data = {
        07544269746D61708E000000424D8E000000000000003E000000280000001400
        0000140000000100010000000000500000000000000000000000020000000000
        000000000000FFFFFF00FFFFF000FFFFF000FFFFF000FFFFF000FFFFF000FFFF
        F000FFFFF000FF000000FF000000FF000000FF1FF000FF1FF000FF1FF000FF1F
        F000FF1FF000FF1FF000FF1FF000FF1FF000FF1FF000FF1FF000}
      Stretch = True
      Transparent = True
    end
    object Image2: TImage
      Left = 220
      Top = 145
      Width = 150
      Height = 55
      Center = True
      Picture.Data = {
        07544269746D61708E000000424D8E000000000000003E000000280000001400
        0000140000000100010000000000500000000000000000000000020000000000
        000000000000FFFFFF00FFFFF000FFFFF000FFFFF000FFFFF000FFFFF000FFFF
        F000FFFFF000000FF000000FF000000FF000FF8FF000FF8FF000FF8FF000FF8F
        F000FF8FF000FF8FF000FF8FF000FF8FF000FF8FF000FF8FF000}
      Stretch = True
      Transparent = True
    end
    object Label10: TLabel
      Left = 59
      Top = 145
      Width = 67
      Height = 13
      Caption = 'Spojovaci ID :'
    end
    object Label11: TLabel
      Left = 320
      Top = 145
      Width = 64
      Height = 13
      Caption = 'Spojovaci ID:'
    end
    object ComboBox3: TComboBox
      Left = 5
      Top = 35
      Width = 200
      Height = 21
      ItemHeight = 13
      TabOrder = 0
      OnSelect = ComboBox3Select
    end
    object ComboBox4: TComboBox
      Left = 245
      Top = 35
      Width = 200
      Height = 21
      ItemHeight = 13
      TabOrder = 1
      OnSelect = ComboBox4Select
    end
    object ComboBox5: TComboBox
      Left = 5
      Top = 80
      Width = 100
      Height = 21
      ItemHeight = 13
      TabOrder = 2
    end
    object ComboBox6: TComboBox
      Left = 245
      Top = 80
      Width = 100
      Height = 21
      ItemHeight = 13
      TabOrder = 3
    end
    object ComboBox7: TComboBox
      Left = 5
      Top = 120
      Width = 200
      Height = 21
      ItemHeight = 13
      TabOrder = 4
    end
    object ComboBox8: TComboBox
      Left = 245
      Top = 120
      Width = 200
      Height = 21
      ItemHeight = 13
      TabOrder = 5
    end
    object ComboBox9: TComboBox
      Left = 5
      Top = 165
      Width = 120
      Height = 21
      ItemHeight = 13
      TabOrder = 6
    end
    object DBGrid1: TDBGrid
      Left = 5
      Top = 225
      Width = 630
      Height = 120
      DataSource = DataSource3
      TabOrder = 7
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
    end
    object Button1: TButton
      Left = 5
      Top = 194
      Width = 200
      Height = 25
      Caption = 'Nahled zdrojove tabulky'
      Enabled = False
      TabOrder = 8
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 245
      Top = 194
      Width = 200
      Height = 25
      Caption = 'Nahled cilove tabulky'
      Enabled = False
      TabOrder = 9
      OnClick = Button2Click
    end
    object ComboBox10: TComboBox
      Left = 320
      Top = 165
      Width = 120
      Height = 21
      ItemHeight = 13
      TabOrder = 10
    end
    object Edit1: TEdit
      Left = 110
      Top = 80
      Width = 100
      Height = 21
      TabOrder = 11
    end
    object Edit2: TEdit
      Left = 350
      Top = 80
      Width = 100
      Height = 21
      TabOrder = 12
    end
    object Button3: TButton
      Left = 185
      Top = 160
      Width = 75
      Height = 25
      Caption = 'Sparovani'
      TabOrder = 13
      OnClick = Button3Click
    end
    object Button5: TButton
      Left = 500
      Top = 15
      Width = 130
      Height = 25
      Caption = 'Nahrat nastaveni'
      TabOrder = 14
      OnClick = Button5Click
    end
    object Button6: TButton
      Left = 500
      Top = 150
      Width = 130
      Height = 25
      Caption = 'Ulozit nastaveni'
      TabOrder = 15
      OnClick = Button6Click
    end
    object Memo2: TMemo
      Left = 500
      Top = 50
      Width = 130
      Height = 90
      ScrollBars = ssVertical
      TabOrder = 16
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 0
    Width = 640
    Height = 65
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 16
      Width = 45
      Height = 13
      Caption = 'Soubor:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 5
      Top = 40
      Width = 75
      Height = 13
      Caption = 'Jazyk zdrojovy: '
    end
    object Label3: TLabel
      Left = 156
      Top = 40
      Width = 42
      Height = 13
      Caption = 'a cilovy: '
    end
    object Label12: TLabel
      Left = 275
      Top = 40
      Width = 129
      Height = 13
      Caption = 'Nastaveni - typ vstupni DB:'
    end
    object ComboBox1: TComboBox
      Left = 81
      Top = 35
      Width = 70
      Height = 21
      ItemHeight = 13
      TabOrder = 0
      Text = 'EN-US'
    end
    object ComboBox2: TComboBox
      Left = 200
      Top = 35
      Width = 70
      Height = 21
      ItemHeight = 13
      TabOrder = 1
      Text = 'CZ'
    end
    object Button4: TButton
      Left = 525
      Top = 30
      Width = 110
      Height = 25
      Caption = 'Vytvoreni txt exportu'
      Enabled = False
      TabOrder = 2
      OnClick = Button4Click
    end
    object Button7: TButton
      Left = 408
      Top = 32
      Width = 75
      Height = 25
      Caption = 'Otevrit...'
      Enabled = False
      TabOrder = 3
      OnClick = Button7Click
    end
  end
  object GroupBox3: TGroupBox
    Left = 0
    Top = 70
    Width = 640
    Height = 100
    Caption = '[ Nahled exportu ]'
    TabOrder = 2
    Visible = False
    object Memo1: TMemo
      Left = 5
      Top = 24
      Width = 630
      Height = 70
      ScrollBars = ssVertical
      TabOrder = 0
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 531
    Width = 640
    Height = 19
    Panels = <>
  end
  object MainMenu1: TMainMenu
    Left = 600
    object NovyexportTM1: TMenuItem
      Caption = 'Subor...'
      object Otvoritdatabazu1: TMenuItem
        Caption = 'Otvorit databazu...'
        OnClick = Otvoritdatabazu1Click
      end
      object Ukoncirprogram1: TMenuItem
        Caption = 'Ukoncit program'
        OnClick = Ukoncirprogram1Click
      end
    end
    object Vice1: TMenuItem
      Caption = 'Vice...'
      object Zobrazitnahledexportu1: TMenuItem
        Caption = 'Nahled exportu'
        OnClick = Zobrazitnahledexportu1Click
      end
      object Zobrazitnastaveniprozdroj1: TMenuItem
        Caption = 'Nastaveni pro zdroj'
        OnClick = Zobrazitnastaveniprozdroj1Click
      end
    end
  end
  object ADOConnection1: TADOConnection
    LoginPrompt = False
    Left = 120
  end
  object ADOQuery1: TADOQuery
    Connection = ADOConnection1
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM en')
    Left = 168
    Top = 176
  end
  object OpenDialog1: TOpenDialog
    Left = 72
  end
  object DataSource1: TDataSource
    DataSet = ADOQuery1
    Left = 200
    Top = 176
  end
  object ADOQuery2: TADOQuery
    Connection = ADOConnection1
    Parameters = <>
    Left = 408
    Top = 176
  end
  object ADOQuery3: TADOQuery
    Connection = ADOConnection1
    Parameters = <>
    Left = 152
  end
  object DataSource2: TDataSource
    DataSet = ADOQuery2
    Left = 440
    Top = 176
  end
  object DataSource3: TDataSource
    DataSet = ADOQuery3
    Left = 184
  end
  object SaveDialog1: TSaveDialog
    Left = 224
  end
end
