unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ComCtrls, StdCtrls, DB, ADODB, CheckLst, ExtCtrls, Grids,
  DBGrids, DBCtrls;

type
  TForm1 = class(TForm)
    MainMenu1: TMainMenu;
    NovyexportTM1: TMenuItem;
    Otvoritdatabazu1: TMenuItem;
    Ukoncirprogram1: TMenuItem;
    ADOConnection1: TADOConnection;
    ADOQuery1: TADOQuery;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    ComboBox3: TComboBox;
    ComboBox4: TComboBox;
    OpenDialog1: TOpenDialog;
    Label6: TLabel;
    Label7: TLabel;
    ComboBox5: TComboBox;
    ComboBox6: TComboBox;
    Label8: TLabel;
    Label9: TLabel;
    ComboBox7: TComboBox;
    ComboBox8: TComboBox;
    ComboBox9: TComboBox;
    Image1: TImage;
    Image2: TImage;
    Label10: TLabel;
    DBGrid1: TDBGrid;
    Button1: TButton;
    Button2: TButton;
    DataSource1: TDataSource;
    Label11: TLabel;
    ComboBox10: TComboBox;
    ADOQuery2: TADOQuery;
    ADOQuery3: TADOQuery;
    DataSource2: TDataSource;
    DataSource3: TDataSource;
    Edit1: TEdit;
    Edit2: TEdit;
    Button3: TButton;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    Label2: TLabel;
    Label3: TLabel;
    Label12: TLabel;
    Button4: TButton;
    GroupBox3: TGroupBox;
    Memo1: TMemo;
    StatusBar1: TStatusBar;
    Vice1: TMenuItem;
    Zobrazitnahledexportu1: TMenuItem;
    Zobrazitnastaveniprozdroj1: TMenuItem;
    Button5: TButton;
    Button6: TButton;
    Memo2: TMemo;
    SaveDialog1: TSaveDialog;
    Button7: TButton;
    procedure Ukoncirprogram1Click(Sender: TObject);
    procedure Otvoritdatabazu1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ComboBox3Select(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure ComboBox4Select(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Zobrazitnahledexportu1Click(Sender: TObject);
    procedure Zobrazitnastaveniprozdroj1Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  dbname: String;

implementation

{$R *.dfm}

procedure TForm1.Ukoncirprogram1Click(Sender: TObject);
begin

  Close;
end;

procedure TForm1.Otvoritdatabazu1Click(Sender: TObject);
begin
  try
    ADOConnection1.Connected := False;
    Button7.Enabled := False;
    OpenDialog1.Filter := 'Multiterm/Access (*.mdb)|*.mdb|Vsechny soubory (*.*)|*.*';
    OpenDialog1.Execute;
    dbname := OpenDialog1.FileName;
    Label1.Caption := 'Soubor: '+dbname;
    if(dbname<>'') then
    begin
    ADOConnection1.ConnectionString := 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source='+dbname+';Persist Security Info=False';
    ADOConnection1.Connected := True;
    ADOConnection1.GetTableNames(ComboBox3.Items, False);
    ADOConnection1.GetTableNames(ComboBox4.Items, False);
    Button7.Enabled := True;
    StatusBar1.SimpleText := 'Zdrojovy soubor otevren...';
    end;
  except
    ShowMessage('Chyba otvorenia suboru !!!');
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  try
    ComboBox1.Items.LoadFromFile('languages');
    ComboBox2.Items.LoadFromFile('languages');
    Memo1.Lines.LoadFromFile('txtHead');
    Form1.Height := 130;
    GroupBox1.Top := 70;
  except
    ShowMessage('Nenalezen subor "languages" se seznamem jazyku !!!');
  end;
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    ADOConnection1.Connected := False;
end;

procedure TForm1.ComboBox3Select(Sender: TObject);
begin
  ADOConnection1.GetFieldNames(ComboBox3.Text, ComboBox5.Items);
  ADOConnection1.GetFieldNames(ComboBox3.Text, ComboBox7.Items);
  ADOConnection1.GetFieldNames(ComboBox3.Text, ComboBox9.Items);
  Button1.Enabled := True;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  try
    ADOQuery3.Active := False;
    ADOQuery3.SQL.Clear;
    ADOQuery3.SQL.Add('SELECT * FROM '+ ComboBox3.Text);
    ADOQuery3.Active := True;
  except
    ShowMessage('Chyba pripojeni ke zdrojove tabulce !!!');
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  try
    ADOQuery3.Active := False;
    ADOQuery3.SQL.Clear;
    ADOQuery3.SQL.Add('SELECT * FROM '+ ComboBox4.Text);
    ADOQuery3.Active := True;
  except
    ShowMessage('Chyba pripojeni k cilove tabulce !!!');
  end;
end;

procedure TForm1.ComboBox4Select(Sender: TObject);
begin
  ADOConnection1.GetFieldNames(ComboBox4.Text, ComboBox6.Items);
  ADOConnection1.GetFieldNames(ComboBox4.Text, ComboBox8.Items);
  ADOConnection1.GetFieldNames(ComboBox4.Text, ComboBox10.Items);
  Button2.Enabled := True;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
   try
     ADOQuery3.Active := False;
     ADOQuery3.SQL.Clear;
     ADOQuery3.SQL.Add('SELECT S.'+ComboBox7.Text+', T.'+ComboBox8.Text+' FROM '+ComboBox3.Text+' AS S, '+ComboBox4.Text+' AS T'+' WHERE S.'+ComboBox9.Text+'=T.'+ComboBox10.Text+' AND S.'+ComboBox5.Text+'='+Edit1.Text+' AND T.'+ComboBox6.Text+'='+Edit2.Text+';');
     ADOQuery3.Active := True;
   except
     ShowMessage('Chyba sparovani - skontrolujte nastaveni !!!');
   end;
end;

procedure TForm1.Zobrazitnahledexportu1Click(Sender: TObject);
begin
  if(GroupBox3.Visible) then
  begin
    Zobrazitnahledexportu1.Checked := False;
    GroupBox3.Visible := False;
    GroupBox1.Top := 70;
    Form1.Height := Form1.Height - GroupBox3.Height;
  end
  else
  begin
    Form1.Height := Form1.Height + GroupBox3.Height;
    GroupBox1.Top := 175;
    GroupBox3.Visible := True;
    Zobrazitnahledexportu1.Checked := True;
  end;
end;

procedure TForm1.Zobrazitnastaveniprozdroj1Click(Sender: TObject);
begin
if(GroupBox1.Visible) then
  begin
    Zobrazitnastaveniprozdroj1.Checked := False;
    GroupBox1.Visible := False;
    Form1.Height := Form1.Height - GroupBox1.Height;
  end
  else
  begin
    Form1.Height := Form1.Height + GroupBox1.Height;
    GroupBox1.Visible := True;
    Zobrazitnastaveniprozdroj1.Checked := True;
  end;
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
  try
    OpenDialog1.Filter := 'Soubory s nastavenim (*.setg)|*.setg|Vsechny soubory (*.*)|*.*';
    OpenDialog1.Execute;
    if(OpenDialog1.FileName<>'') then
    begin
    Memo2.Lines.LoadFromFile(OpenDialog1.FileName);
    // zdroj
    ComboBox3.Text := Memo2.Lines[0];
    ComboBox5.Text := Memo2.Lines[1];
    Edit1.Text := Memo2.Lines[2];
    ComboBox7.Text := Memo2.Lines[3];
    ComboBox9.Text := Memo2.Lines[4];
    // ciel
    ComboBox4.Text := Memo2.Lines[5];
    ComboBox6.Text := Memo2.Lines[6];
    Edit2.Text := Memo2.Lines[7];
    ComboBox8.Text := Memo2.Lines[8];
    ComboBox10.Text := Memo2.Lines[9];
    end;
  except
    ShowMessage('Chyba v souboru s nastavenim !!!');
  end;
end;

procedure TForm1.Button6Click(Sender: TObject);
begin
try
    // zdroj
    Memo2.Lines.Add(ComboBox3.Text);
    Memo2.Lines.Add(ComboBox5.Text);
    Memo2.Lines.Add(Edit1.Text);
    Memo2.Lines.Add(ComboBox7.Text);
    Memo2.Lines.Add(ComboBox9.Text);
    // ciel
    Memo2.Lines.Add(ComboBox4.Text);
    Memo2.Lines.Add(ComboBox6.Text);
    Memo2.Lines.Add(Edit2.Text);
    Memo2.Lines.Add(ComboBox8.Text);
    Memo2.Lines.Add(ComboBox10.Text);
    SaveDialog1.Filter := 'Soubory s nastavenim (*.setg)|*.setg|Vsechny soubory (*.*)|*.*';
    SaveDialog1.Execute;
    if(SaveDialog1.FileName<>'') then
      Memo2.Lines.SaveToFile(SaveDialog1.FileName+'.setg');
  except
    ShowMessage('Chyba v souboru s nastavenim !!!');
  end;
end;

procedure TForm1.Button4Click(Sender: TObject);
var i,max: Integer;
begin
  Button3.Click;
  ADOQuery3.First;
  while NOT ADOQuery3.Eof do
  begin
    Memo1.Lines.Add('<TrU>');
    Memo1.Lines.Add('<Seg L='+ComboBox1.Text+'>'+ADOQuery3.Fields[0].AsString);
    Memo1.Lines.Add('<Seg L='+ComboBox2.Text+'>'+ADOQuery3.Fields[1].AsString);
    Memo1.Lines.Add('</TrU>');
    ADOQuery3.Next;
  end;
  try
    SaveDialog1.Filter := 'TM export (*.txt)|*.txt';
    SaveDialog1.FileName := dbname+'.txt';
    SaveDialog1.Execute;
    Memo1.Lines.SaveToFile(SaveDialog1.FileName);
  except
    ShowMessage('Nepodarilo se ulozit export !!!');
  end;
end;

procedure TForm1.Button7Click(Sender: TObject);
begin
  try
  Button5.Click;
  Button4.Enabled := True;
  StatusBar1.SimpleText := 'Soubor s nastavenimi pouzit...';
  except
  Button4.Enabled := False;
  end;
end;

end.
